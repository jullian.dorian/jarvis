package fr.snyker;

import fr.snyker.api.Api;
import fr.snyker.commands.CommandReply;
import fr.snyker.commands.CommandRestart;
import fr.snyker.interaction.Interaction;
import fr.snyker.listeners.MemberEvent;
import fr.snyker.listeners.MessageEvent;
import fr.snyker.resources.Resource;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;

import javax.security.auth.login.LoginException;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Class créée le 08/06/2018 à 08:55
 * par Jullian Dorian
 */
public class Jarvis {

    public static JDA JDA;
    public static Api API;
    public static Interaction INTERACTION;

    private Resource resource;

    /**
     * Instancie les variables public
     */
    Jarvis() throws Exception {
        resource = new Resource();
        resource.createFoldersAndFiles();

        if(resource.getDatabase().getFile() == null || !resource.getDatabase().getFile().exists()){
            System.out.println("Le fichier database.json est introuvable dans le dossier : \\Jarvis\\database.json\nArrêt du programme");
            System.exit(0);
        }

        JDA = new JDABuilder(AccountType.BOT)
                .setToken("NDU0NTM1Nzc1Mjk2NTUyOTY4.Dfu_HQ.0FnsE2hB9EWWVZkdEbUsC4tAE3I")
                .buildBlocking();

        /* SETTER */
        API = new Api(resource.getDatabase().getFile());
        INTERACTION = new Interaction();
    }

    /**
     * Lance, exécute les classes
     */
    void run(){
        API.addCommand(new CommandRestart());
        API.addCommand(new CommandReply());

        JDA.addEventListener((Object[]) API.getEvents());
        JDA.addEventListener(new MemberEvent(), new MessageEvent());

        INTERACTION.init();

        boolean running = true;
        Scanner scanner = new Scanner(System.in);
        while(running){
            if(scanner.nextLine().equalsIgnoreCase("stop"))
                running = false;
        }
        System.out.println("Arrêt en cours du programme.\nSauvegarde des données en cours...");
        Runnable runnable = () -> {
            System.out.println("Runnable");
            System.out.println("Waiting 5s");
            try {
                Thread.sleep(5000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Ok !");
        };
        runnable.run();
        System.out.println("Shutdown");
        JDA.shutdown();
    }

}
