package fr.snyker.api.listeners;

/**
 * Class créée le 08/06/2018 à 09:50
 * par Jullian Dorian
 */
public class EventListener{

    public EventListener(){

    }

    public net.dv8tion.jda.core.hooks.EventListener[] callEvents() {
        return new net.dv8tion.jda.core.hooks.EventListener[]{
                new CommandEvent()
        };
    }
}
