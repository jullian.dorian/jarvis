package fr.snyker.api.listeners;

import fr.snyker.Jarvis;
import fr.snyker.api.Api;
import fr.snyker.api.commands.CommandExecutor;
import fr.snyker.api.utils.Messages;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import net.dv8tion.jda.core.hooks.SubscribeEvent;

import java.sql.SQLException;

/**
 * Class créée le 08/06/2018 à 10:17
 * par Jullian Dorian
 */
public class CommandEvent extends ListenerAdapter {

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {

        Member member = event.getMember();

        if(event.getAuthor().isBot()) return;

        System.out.println("CommandEvent - MessageReceived");

        //On parcours la liste des commandes
        for(CommandExecutor executor : Api.commands){
            //On vérifie si la commande en est une
            if(executor.isCommand(event.getMessage())){
                //On vérifie bien que celui qui éxécute la commande n'est pas un bot
                if(!executor.senderIsBot(member)){
                    //Si le joueur à la permission on effectue la commande, sinon on renvoie un message d'erreur
                    if(executor.userHasPermission(member)) executor.call(event, member);
                    else executor.sendMessage(event.getTextChannel(), Messages.COMMAND_NO_PERMISSION);
                }
                break;
            }
        }

    }
}
