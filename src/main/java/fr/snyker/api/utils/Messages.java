package fr.snyker.api.utils;

/**
 * Class créée le 08/06/2018 à 10:33
 * par Jullian Dorian
 */
public enum Messages {

    COMMAND_NO_PERMISSION("Désolé Monsieur, vos droits ne suffisent pas pour que je vous vienne en aide.");

    private String message;

    Messages(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
