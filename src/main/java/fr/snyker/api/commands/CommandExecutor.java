package fr.snyker.api.commands;

import fr.snyker.Jarvis;
import fr.snyker.api.utils.Messages;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.Event;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.util.Arrays;

/**
 * Class créée le 08/06/2018 à 09:36
 * par Jullian Dorian
 */
public abstract class CommandExecutor implements ICommand{

    public CommandExecutor(){

    }

    /**
     * Vérifie si le début du message commence par le tag de la commande, la commande en elle même
     * ou un aliase
     * @param message - Le message reçu
     * @return true - si le tag est au début.
     */
    public boolean isCommand(Message message) {
        String content = message.getContentDisplay();

        if(content.startsWith(this.getTag())){

            String[] words = content.split(" ");
            String name = words[0].substring(1);

            if(this.getName().equalsIgnoreCase(name))
                return true;
            else {
                for (String aliase : this.getAliases()) {
                    if(aliase.equalsIgnoreCase(name))
                        return true;
                }
            }
        }

        return false;
    }

    /**
     * Vérifie si l'utilisateur est un bot
     * @param member - L'utilisateur à vérifier
     * @return true - si l'utilisateur est un bot
     */
    public boolean senderIsBot(Member member) {
        return member.getUser().isBot();
    }

    /**
     * Vérifie si un utilisateur à la permission de faire la commande
     * @param member
     * @return true - si l'utilisateur à la permission
     */
    public boolean userHasPermission(Member member) {
        for(Permission permission : getRequiredPerms()){
            if(!member.getPermissions().contains(permission))
                return false;
        }
        return true;
    }

    /**
     * Renvoie une autre fonction "execute" en passant en paramètre
     * Le TextChannel du message, L'utilisateur qui l'a envoyé et les arguments de la commande.
     *
     * @see #execute(MessageChannel, Member, String[])
     * @param user - L'utilisateur qui a fait la commande
     */
    public void call(MessageReceivedEvent event, Member user) {
        String message = event.getMessage().getContentRaw();

        String split[] = message.split(" ");
        String args[] = new String[split.length-1];

        System.arraycopy(split, 1, args, 0, split.length - 1);

        execute(event.getTextChannel(), user, args);
    }

    public void sendMessage(MessageChannel messageChannel, String message){
        if(messageChannel instanceof TextChannel)
            messageChannel.sendMessage(message).queue();
        else if(messageChannel instanceof PrivateChannel) {
            PrivateChannel privateChannel = (PrivateChannel) messageChannel;
            if(!privateChannel.isFake()){
                privateChannel.sendMessage(message).queue();
            }
        }
    }

    public void sendMessage(MessageChannel messageChannel, Messages messages) {
        sendMessage(messageChannel, messages.getMessage());
    }

    public void sendMessage(MessageChannel messageChannel, Messages messages, Member member) {
        if(messageChannel instanceof TextChannel)
            messageChannel.sendMessage(String.format("%s %s", member.getAsMention(), messages.getMessage())).queue();
        else if(messageChannel instanceof PrivateChannel) {
            member.getUser().openPrivateChannel().queue(privateChannel -> {
                privateChannel.sendMessage(String.format("%s %s", member.getAsMention(), messages.getMessage())).queue();
                privateChannel.close().complete();
            });
        }
    }
}
