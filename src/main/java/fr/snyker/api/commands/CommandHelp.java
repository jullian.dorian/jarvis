package fr.snyker.api.commands;

import fr.snyker.api.Api;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.MessageChannel;

import java.util.Arrays;
import java.util.List;

/**
 * Class créée le 08/06/2018 à 10:16
 * par Jullian Dorian
 */
public class CommandHelp extends CommandExecutor {
    /**
     * Retourne le nom de la commande tapé.
     *
     * @return name
     */
    public String getName() {
        return "aide";
    }

    /**
     * Retourne les aliases liés à la commande
     *
     * @return aliases
     */
    public String[] getAliases() {
        return new String[]{"help", "h", "?"};
    }

    /**
     * Retourne la description de la commande (Pour l'aide)
     *
     * @return description
     */
    public String getDescription() {
        return "Affiche la liste des commandes.";
    }

    @Override
    public List<Permission> getRequiredPerms() {
        return Arrays.asList(Permission.EMPTY_PERMISSIONS);
    }

    /**
     * Exécutor de la commande, tout se passe ici pour faire la commande
     *
     * @param channel
     * @param member
     * @param args
     */
    @Override
    public void execute(MessageChannel channel, Member member, String[] args) {
        member.getUser().openPrivateChannel().queue(privateChannel -> {
            privateChannel.sendMessage("Voici la liste des commandes : ").queue();

            StringBuilder stringBuilder = new StringBuilder();
            for(CommandExecutor executor : Api.commands){
                stringBuilder.append(executor.getName()).append(" : ").append(executor.getDescription());
                stringBuilder.append("\n");
            }

            privateChannel.sendMessage(stringBuilder.toString()).queue();

            privateChannel.close().complete();
        });
    }

}
