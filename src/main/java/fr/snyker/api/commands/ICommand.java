package fr.snyker.api.commands;

import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;

import java.util.List;

/**
 * Class créée le 08/06/2018 à 09:34
 * par Jullian Dorian
 */
public interface ICommand {

    /**
     * Retourne le nom de la commande tapé.
     * @return name
     */
    String getName();

    /**
     * Retourne les aliases liés à la commande
     * @return aliases
     */
    String[] getAliases();

    /**
     * Retourne la description de la commande (Pour l'aide)
     * @return description
     */
    String getDescription();

    /**
     * Retourne la liste des permissions de la commande pour qu'elle soit affiché
     * @return permissions
     */
    List<Permission> getRequiredPerms();

    /**
     * Retourne par défaut le tag des commandes, peut être changé selon la commande
     * @return tag
     */
    default String getTag(){
        return ".";
    }

    /**
     * Exécutor de la commande, tout se passe ici pour faire la commande.
     * @param channel - Le channel ou la commande a été envoyer
     * @param user - L'utilisateur qui a éxécuté la commande
     * @param args - Les arguments de la commande
     */
    void execute(MessageChannel channel, Member user, String[] args);

}
