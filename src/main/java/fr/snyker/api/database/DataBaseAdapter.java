package fr.snyker.api.database;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

/**
 * Class créée le 22/06/2018 à 16:51
 * par Jullian Dorian
 */
public class DataBaseAdapter extends TypeAdapter<DataBase> {

    @Override
    public void write(JsonWriter jsonWriter, DataBase dataBase) throws IOException {

        jsonWriter.beginObject();

        jsonWriter.name("driver").value(dataBase.getDriver());
        jsonWriter.name("host").value(dataBase.getHost());
        jsonWriter.name("port").value(dataBase.getPort());
        jsonWriter.name("dbname").value(dataBase.getDbname());
        jsonWriter.name("user").value(dataBase.getUser());
        jsonWriter.name("password").value(dataBase.getPassword());

        jsonWriter.endObject();

    }

    @Override
    public DataBase read(JsonReader jsonReader) throws IOException {

        String driver = null;
        String host = null;
        int port = 0;
        String dbname = null;
        String user = null;
        String password = null;

        jsonReader.beginObject();
        while (jsonReader.hasNext()){
            switch (jsonReader.nextName()){
                case "driver":
                    driver = jsonReader.nextString();
                    break;
                case "host":
                    host = jsonReader.nextString();
                    break;
                case "port":
                    port = jsonReader.nextInt();
                    break;
                case "dbname":
                    dbname = jsonReader.nextString();
                    break;
                case "user":
                    user = jsonReader.nextString();
                    break;
                case "password":
                    password = jsonReader.nextString();
                    break;
            }

        }
        jsonReader.endObject();

        return new DataBase(driver, host, port, dbname, user, password);
    }
}
