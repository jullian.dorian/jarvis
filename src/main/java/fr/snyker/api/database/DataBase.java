package fr.snyker.api.database;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.snyker.api.Api;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.User;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Class créée le 22/06/2018 à 16:46
 * par Jullian Dorian
 */
public class DataBase {

    private String driver;
    private String host;
    private int port;
    private String dbname;
    private String user;
    private String password;

    private Connection connection;

    public DataBase(){}

    public DataBase(String driver, String host, int port, String dbname, String user, String password){
        this.driver = driver;
        this.host = host;
        this.port = port;
        this.dbname = dbname;
        this.user = user == null ? "root" : user;
        this.password = password == null ? "" : password;
    }

    /**
     * Connecte la base de données
     * @return true - si la connexion a bien été faite
     */
    public boolean connect(){
        try {
            final String url = driver+"://" + host + ":" + port + "/" + dbname + "?autoReconnect=true&useSSL=false";
            Class.forName("com.mysql.jdbc.Driver");
            this.connection = DriverManager.getConnection(url, user, password);
            System.out.println("Connexion établit avec la base de données.");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Impossible de se connecter.");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Déconnecte la base de donnée
     * @return true - si la connexion a bien été fermer
     */
    public boolean disconnect(){
        if(this.connection != null){
            try {
                this.connection.close();
                this.connection = null;
                System.out.println("La base de données a été fermer.");
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("Impossible de se deconnecter.");
            }
        }
        return false;
    }

    /**
     * Retourne la connection actuel de la base de données
     * @return connection
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     *
     */
    public void createEmptyTables() {
        connect();
        //Creation des tables

        disconnect();
    }

    /**
     * Charge les variables nécessaires pour être connecté à la base de données.
     */
    public void load(Api api, File file) {
        Gson gson = new GsonBuilder()
                .disableHtmlEscaping()
                .setPrettyPrinting()
                .serializeNulls()
                .registerTypeAdapter(DataBase.class, new DataBaseAdapter())
                .create();

        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while((line = bufferedReader.readLine()) != null){
                stringBuilder.append(line);
            }

            String content = stringBuilder.toString();

            api.setDataBase((DataBase) gson.fromJson(content, DataBase.class));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Crée un nouveau utilisateur dans la table
     * @param member - Le membre à crée
     * @throws SQLException
     */
    public void createUser(Member member) throws SQLException {
        connect();

        User user = member.getUser();

        PreparedStatement stmt;
        //Selection de l'id user
        stmt = connection.prepareStatement("SELECT COUNT(id_user) FROM users WHERE id_user=?");
        stmt.setLong(1, user.getIdLong());

        ResultSet resultSet = stmt.executeQuery();
        resultSet.last();
        int count = resultSet.getInt(1);
        resultSet.beforeFirst();
        stmt.close();

        if(count == 0){
            stmt = connection.prepareStatement("INSERT INTO users (id_user, name) " +
                    "VALUES ('"+member.getUser().getIdLong()+"', '"+member.getUser().getName()+"')");
            stmt.executeUpdate();
            System.out.println("Utilisateur: "+member.getUser().getName()+" a été crée.");

            stmt.close();
        }

        disconnect();
    }

    /**
     * Supprime l'utilisateur de toutes les tables
     * @param member - L'utilisateur à supprimer
     * @throws SQLException
     */
    public void removeUser(Member member) throws SQLException{
        connect();

        User user = member.getUser();

        PreparedStatement stmt;
        //Selection de l'id user
        stmt = connection.prepareStatement("SELECT COUNT(id_user) FROM users WHERE id_user=?");
        stmt.setLong(1, user.getIdLong());

        ResultSet resultSet = stmt.executeQuery();
        resultSet.last();
        int count = resultSet.getInt(1);
        resultSet.beforeFirst();
        stmt.close();

        if(count == 1){
            stmt = connection.prepareStatement("DELETE FROM users WHERE id_user=?");
            stmt.setLong(1, user.getIdLong());
            stmt.executeUpdate();
            System.out.println("Utilisateur: "+member.getUser().getName()+" a été supprimer.");

            stmt.close();
        }

        disconnect();
    }


    public void addReply(int id, String message, boolean ignoreCase) throws SQLException{
        connect();

        PreparedStatement stmt;
        stmt = connection.prepareStatement("INSERT INTO responses (response, ignoreCase, id_question) VALUES (?,?,?)");
        stmt.setString(1, message);
        stmt.setBoolean(2, ignoreCase);
        stmt.setInt(3, id);
        stmt.executeUpdate();
        stmt.close();

        disconnect();
    }

    /**
     *
     * @param message
     * @return
     * @throws SQLException
     */
    public int createQuestion(String message) throws SQLException{
        connect();

        message = message.substring(0, message.length()-1);

        PreparedStatement stmt;
        stmt = connection.prepareStatement("INSERT INTO questions (question) VALUES (?)");
        stmt.setString(1, message);
        stmt.executeUpdate();
        stmt.close();

        int id = 0;
        stmt = connection.prepareStatement("SELECT id_question FROM questions WHERE question=? ORDER BY id_question DESC LIMIT 1");
        stmt.setString(1, message);
        ResultSet result = stmt.executeQuery();
        result.last();
        id = result.getInt("id_question");
        result.beforeFirst();
        result.close();

        disconnect();
        return id;
    }

    public String getDriver() {
        return driver == null ? "jdbc:mysql" : driver;
    }

    public String getHost() {
        return host == null ? "localhost" : host;
    }

    public int getPort() {
        return port == 0 ? 3306 : port;
    }

    public String getDbname() {
        return dbname == null ? "jarvis" : dbname;
    }

    public String getUser() {
        return user == null ? "root" : user;
    }

    public String getPassword() {
        return password == null ? "" : password;
    }
}
