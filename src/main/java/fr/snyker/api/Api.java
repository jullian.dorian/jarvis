package fr.snyker.api;

import fr.snyker.api.commands.CommandExecutor;
import fr.snyker.api.commands.CommandHelp;
import fr.snyker.api.database.DataBase;
import fr.snyker.api.listeners.EventListener;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class créée le 08/06/2018 à 09:51
 * par Jullian Dorian
 */
public class Api {

    /* DataBase de l'api */
    private DataBase dataBase;
    public static List<CommandExecutor> commands;

    public Api(File database){

        this.dataBase = new DataBase();
        this.dataBase.load(this, database);
        //this.dataBase.createEmptyTables();

        commands = new ArrayList<>();

        addCommand(new CommandHelp());
    }

    /**
     * Retourne la base de données
     * @return database
     */
    public DataBase getDataBase() {
        return dataBase;
    }

    public void setDataBase(DataBase dataBase){
        this.dataBase = dataBase;
    }

    /**
     * Ajoute une nouvelle commande
     * @param command - La commande à ajouter
     */
    public void addCommand(CommandExecutor command){
        commands.add(command);
    }

    /**
     * Ajoute une liste de commande.
     * @param commands - Liste des commandes à ajouter
     */
    public void addCommands(CommandExecutor... commands){
        Arrays.stream(commands).forEach(this::addCommand);
    }

    /**
     * Retourne la liste des listeners
     * @return listeners
     */
    public net.dv8tion.jda.core.hooks.EventListener[] getEvents(){
        return new EventListener().callEvents();
    }

}
