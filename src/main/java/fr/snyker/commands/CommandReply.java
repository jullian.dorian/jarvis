package fr.snyker.commands;

import fr.snyker.Jarvis;
import fr.snyker.api.commands.CommandExecutor;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.MessageChannel;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

/**
 * Class créée le 23/06/2018 à 16:54
 * par Jullian Dorian
 */
public class CommandReply extends CommandExecutor {
    /**
     * Retourne le nom de la commande tapé.
     *
     * @return name
     */
    @Override
    public String getName() {
        return "reply";
    }

    /**
     * Retourne les aliases liés à la commande
     *
     * @return aliases
     */
    @Override
    public String[] getAliases() {
        return new String[]{"repondre","rep"};
    }

    /**
     * Retourne la description de la commande (Pour l'aide)
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return "Aucune description pour le moment";
    }

    /**
     * Retourne la liste des permissions de la commande pour qu'elle soit affiché
     *
     * @return permissions
     */
    @Override
    public List<Permission> getRequiredPerms() {
        return Arrays.asList(Permission.ADMINISTRATOR);
    }

    /**
     * Exécutor de la commande, tout se passe ici pour faire la commande.
     *
     * @param channel - Le channel ou la commande a été envoyer
     * @param user    - L'utilisateur qui a éxécuté la commande
     * @param args    - Les arguments de la commande
     */
    @Override
    public void execute(MessageChannel channel, Member user, String[] args) {

        if(args.length > 0){
            switch (args[0]){
                case "help":
                    break;
                case "create":
                    StringBuilder stringBuilder = new StringBuilder();

                    if(args.length <= 2) {
                        sendMessage(channel, "Vous devez écrire une question.");
                        return;
                    }

                    for (int i = 1; i < args.length; i++) {
                        stringBuilder.append(args[i]);
                        stringBuilder.append(" ");
                    }
                    int id = 0;
                    try {
                        id = Jarvis.API.getDataBase().createQuestion(stringBuilder.toString());
                        sendMessage(channel, "La question a été enregistré sous l'id : " + id);
                    } catch (SQLException e) {
                        e.printStackTrace();
                        sendMessage(channel, "Un problème est survenu !");
                    }
                    break;
                case "add":
                    try{
                        id = Integer.parseInt(args[1]);
                    } catch (NumberFormatException e){
                        e.getMessage();
                        sendMessage(channel, "L'argument n'est pas un chiffre !");
                        return;
                    }

                    stringBuilder = new StringBuilder();
                    for (int i = 2; i < args.length; i++) {
                        stringBuilder.append(args[i]);
                        stringBuilder.append(" ");
                    }
                    try {
                        Jarvis.API.getDataBase().addReply(id, stringBuilder.toString(), true);
                        sendMessage(channel, "Le nouveau message a bien été ajouté.");
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    break;
                case "remove":

                    break;
                case "list":

                    break;
                default:
                    channel.sendMessage(new MessageBuilder("L'argument est invalide : " + args[0]).build()).queue();
                    break;
            }
        }

    }
}
