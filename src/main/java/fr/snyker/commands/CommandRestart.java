package fr.snyker.commands;

import fr.snyker.api.commands.CommandExecutor;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.MessageChannel;

import java.util.Arrays;
import java.util.List;

/**
 * Class créée le 22/06/2018 à 15:49
 * par Jullian Dorian
 */
public class CommandRestart extends CommandExecutor{
    /**
     * Retourne le nom de la commande tapé.
     *
     * @return name
     */
    @Override
    public String getName() {
        return "restart";
    }

    /**
     * Retourne les aliases liés à la commande
     *
     * @return aliases
     */
    @Override
    public String[] getAliases() {
        return new String[]{"redemarrer"};
    }

    /**
     * Retourne la description de la commande (Pour l'aide)
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return "Permet de redémarrer le bot";
    }

    /**
     * Retourne la liste des permissions de la commande pour qu'elle soit affiché
     *
     * @return permissions
     */
    @Override
    public List<Permission> getRequiredPerms() {
        return Arrays.asList(Permission.ADMINISTRATOR);
    }

    /**
     * Exécutor de la commande, tout se passe ici pour faire la commande
     *
     * @param channel
     * @param user
     * @param args
     */
    @Override
    public void execute(MessageChannel channel, Member user, String[] args) {
        sendMessage(channel, "Redémarrage en cours...");
        sendMessage(channel, "Fini");
    }
}
