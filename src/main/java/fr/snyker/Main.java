package fr.snyker;

/**
 * Class créée le 08/06/2018 à 08:55
 * par Jullian Dorian
 */
public class Main {

    public static void main(String[] args) throws Exception {
        Jarvis jarvis = new Jarvis();

        jarvis.run();
    }

}