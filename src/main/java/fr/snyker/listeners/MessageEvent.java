package fr.snyker.listeners;

import fr.snyker.Jarvis;
import fr.snyker.interaction.Interaction;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.util.Arrays;

/**
 * Class créée le 23/06/2018 à 16:28
 * par Jullian Dorian
 */
public class MessageEvent extends ListenerAdapter{

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {

        Member member = event.getMember();

        if(member.getUser().isBot()) return;

        Interaction interaction = Jarvis.INTERACTION;
        String message = event.getMessage().getContentRaw();
        if(interaction.isAnInteraction(message)){
            interaction.reply(event.getTextChannel(), message, member);
        }
    }
}
