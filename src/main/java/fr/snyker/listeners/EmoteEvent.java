package fr.snyker.listeners;

import net.dv8tion.jda.core.entities.Emote;
import net.dv8tion.jda.core.events.emote.EmoteAddedEvent;
import net.dv8tion.jda.core.events.emote.EmoteRemovedEvent;
import net.dv8tion.jda.core.events.emote.update.EmoteUpdateNameEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

/**
 * Class créée le 08/06/2018 à 22:14
 * par Jullian Dorian
 */
public class EmoteEvent extends ListenerAdapter {

    @Override
    public void onEmoteAdded(EmoteAddedEvent event){

    }


    @Override
    public void onEmoteRemoved(EmoteRemovedEvent event){

    }


    @Override
    public void onEmoteUpdateName(EmoteUpdateNameEvent event){

    }

}
