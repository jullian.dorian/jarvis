package fr.snyker.interaction;

import fr.snyker.Jarvis;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.TextChannel;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class créée le 23/06/2018 à 16:44
 * par Jullian Dorian
 */
public class Interaction {

    private List<Interact> interacts = new ArrayList<>();

    public Interaction(){}

    /**
     * Ajoute toutes les questions et réponse dans la liste
     * @see #interacts
     */
    public void init() {
        Jarvis.API.getDataBase().connect();

        try{
            String URI = "SELECT * FROM questions ORDER BY id_question ASC";
            PreparedStatement statement = Jarvis.API.getDataBase().getConnection().prepareStatement(URI);
            ResultSet result = statement.executeQuery();

            while(result.next()){
                Question question = new Question(result.getInt(1), result.getString(2));

                String URI2 = "SELECT id_response, response, ignoreCase FROM responses WHERE id_question=?";
                PreparedStatement statement2 = Jarvis.API.getDataBase().getConnection().prepareStatement(URI2);
                statement2.setInt(1, question.getId());
                ResultSet resultRep = statement2.executeQuery();

                List<Reponse> reponses = new ArrayList<>();

                while(resultRep.next()){
                    Reponse reponse = new Reponse(resultRep.getInt(1), resultRep.getString(2), resultRep.getBoolean(3));
                    reponse.setQuestion(question);
                    reponses.add(reponse);
                }
                resultRep.close();
                statement2.close();

                Interact interact = new Interact(question, reponses);
                this.interacts.add(interact);
            }

            result.close();
            statement.close();

        } catch (SQLException e){
            e.getMessage();
        }

        Jarvis.API.getDataBase().disconnect();
    }

    /**
     * Retourne vrai ou faux si il y'a interaction avec le bot.
     * Pour qu'il y est interaction le pseudo du bot doit parraitre au début de la phrase.
     * @param message - Message
     * @return vrai - Si le bot est mentionner en début de phrase.
     */
    public boolean isAnInteraction(String message) {
        String[] args = message.split(" ", 2);

        return args[0].equalsIgnoreCase(Jarvis.JDA.getSelfUser().getAsMention()) || args[0].equalsIgnoreCase("jarvis");
    }

    /**
     * Renvoie un message random en fonction de la question
     * @param channel - channel à renvoyer le message
     * @param message - La question
     * @param member - Le membre
     */
    public void reply(TextChannel channel, String message, Member member) {
        String[] strings = message.split(" ");
        String[] args = new String[strings.length-1];
        System.arraycopy(strings, 1, args, 0, strings.length-1);
        StringBuilder msg = new StringBuilder();
        for (String s : args) {
            msg.append(s);
            msg.append(" ");
        }
        //Nouveau message
        message = msg.toString();

        Interact interact = findByMessage(message);
        String reponse = interact.randomReponse();

        channel.sendMessage(new MessageBuilder(member.getAsMention()+" - "+reponse).build()).queue();
    }

    /**
     * Permet de retrouver une interaction parmis le message.
     * On parcours toutes les interactions jusqu'a trouver une question concordant
     * avec le message.
     * @param message - Le message
     * @return interact
     */
    private Interact findByMessage(String message) {

        if(interacts.isEmpty()) return null;

        for (Interact interact : interacts){
            String question = interact.getQuestion().getQuestion();

            if(matches(question, message, 0.70f)
                    || question.equalsIgnoreCase(message)){
                return interact;
            }

        }
        //ON renvoie le défaut
        return interacts.get(0);
    }

    /**
     * Permet de faire le match d'une question avec le message,
     * un pourcentage est effective pour le niveau d'erreur minimum.
     * @param question - La question
     * @param message - Le message
     * @param percentage - Le pourcentage d'erreur
     * @return true - si le niveau d'erreur est inferieur à la longueur de la question
     */
    private boolean matches(String question,String message, float percentage){
        char[] chars_question = question.toLowerCase().toCharArray();
        char[] chars_message = message.toLowerCase().toCharArray();

        int good = 0;

        for(int i = 0; i < chars_message.length; i++){

            char c_m = chars_message[i];

            for(int j = i, k = 0; j < chars_question.length && k < 6; j++, k++){

                char c_q = chars_question[j];

                if(c_m == c_q){
                    good++;
                    break;
                }

            }

        }

        int min = Math.round(chars_question.length * percentage);
        return good >= min;
    }

}
