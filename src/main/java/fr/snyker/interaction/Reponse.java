package fr.snyker.interaction;

/**
 * Class créée le 30/06/2018 à 12:39
 * par Jullian Dorian
 */
public class Reponse {

    private int id;
    private String reponse;
    private boolean ignoreCase;
    private Question question;

    public Reponse(int id, String reponse, boolean ignoreCase) {
        this.id = id;
        this.reponse = reponse;
        this.ignoreCase = ignoreCase;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReponse() {
        return reponse;
    }

    public void setReponse(String reponse) {
        this.reponse = reponse;
    }

    public boolean isIgnoreCase() {
        return ignoreCase;
    }

    public void setIgnoreCase(boolean ignoreCase) {
        this.ignoreCase = ignoreCase;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }
}
