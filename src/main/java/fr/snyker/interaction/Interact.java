package fr.snyker.interaction;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Class créée le 30/06/2018 à 12:40
 * par Jullian Dorian
 */
public class Interact {

    private Question question;
    private List<Reponse> reponses;

    public Interact(Question question, List<Reponse> reponses) {
        this.question = question;
        this.reponses = reponses;
    }

    public Question getQuestion() {
        return question;
    }

    public boolean addReponse(Reponse reponse){
        return reponses.add(reponse);
    }

    public boolean removeReponse(Reponse reponse){
        return reponses.remove(reponse);
    }

    public List<Reponse> getReponses() {
        return reponses;
    }

    /**
     * Renvoie une réponse aléatoire parmis toutes les réponses de  la question
     * @return message - random
     */
    public String randomReponse() {
        ThreadLocalRandom random = ThreadLocalRandom.current();

        if(getReponses().size() == 0){
            return "Je n'ai aucune réponse pour cette question.";
        }

        int i = random.nextInt(0, getReponses().size());

        return getReponses().get(i).getReponse();
    }
}
