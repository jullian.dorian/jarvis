package fr.snyker.resources;

import java.io.IOException;

/**
 * Class créée le 01/07/2018 à 17:00
 * par Jullian Dorian
 */
public class Resource {

    private ResourceLocation baseFolder;
    private DatabaseResource database;

    private String path;

    public Resource(){
        this.path = "";
    }

    public void createFoldersAndFiles() throws IOException {
        baseFolder = new ResourceLocation(this.path, "Jarvis", false);
        baseFolder.createResource();

        database = new DatabaseResource(baseFolder.getPathCanocinal()+"/", "database.json", true);
        database.createResource();
    }

    public ResourceLocation getBaseFolder() {
        return baseFolder;
    }

    public DatabaseResource getDatabase() {
        return database;
    }
}
