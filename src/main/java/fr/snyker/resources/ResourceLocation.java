package fr.snyker.resources;

import java.io.File;
import java.io.IOException;

/**
 * Class créée le 01/07/2018 à 17:02
 * par Jullian Dorian
 */
public class ResourceLocation {

    private String path;
    private String pathCanocinal;
    private String name;
    private boolean isFile;
    private File file;

    public ResourceLocation(String path, String name, boolean isFile){
        this.path = path;
        this.name = name;
        this.isFile = isFile;
    }

    public boolean createResource() throws IOException {
        File file = new File(path + name);

        this.file = file;
        this.pathCanocinal = file.getCanonicalPath();

        if(isFile){
            if(file.exists())
                return true;
            return file.createNewFile();
        } else {
            return file.mkdirs();
        }

    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPathCanocinal(String pathCanocinal) {
        this.pathCanocinal = pathCanocinal;
    }

    public String getPathCanocinal() {
        return pathCanocinal;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }

    public boolean isFile() {
        return isFile;
    }
}
