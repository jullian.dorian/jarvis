package fr.snyker.resources;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.snyker.api.database.DataBase;
import fr.snyker.api.database.DataBaseAdapter;

import java.io.*;

/**
 * Class créée le 01/07/2018 à 17:26
 * par Jullian Dorian
 */
public class DatabaseResource extends ResourceLocation {

    public DatabaseResource(String path, String name, boolean isFile) {
        super(path, name, isFile);
    }

    @Override
    public boolean createResource() throws IOException {
        File file = new File(getPath() + getName());

        setFile(file);
        setPathCanocinal(file.getCanonicalPath());

        if(isFile()){
            if(file.exists())
                return true;

            boolean b = file.createNewFile();
            setContent();
            return b;
        } else {
            return file.mkdirs();
        }
    }

    private void setContent() {

        Gson gson = new GsonBuilder()
                .disableHtmlEscaping()
                .setPrettyPrinting()
                .serializeNulls()
                .registerTypeAdapter(DataBase.class, new DataBaseAdapter())
                .create();

        try {
            FileWriter fileWriter = new FileWriter(getFile());

            DataBase dataBase = new DataBase();
            String json = gson.toJson(dataBase);

            fileWriter.write(json);
            fileWriter.flush();
            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
